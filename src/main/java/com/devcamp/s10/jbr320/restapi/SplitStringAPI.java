package com.devcamp.s10.jbr320.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
public class SplitStringAPI {
    @CrossOrigin
    @GetMapping("/Split")
    public ArrayList<String> splitString(@RequestParam(required = true) String inpStr) {
        ArrayList<String> ListStrSplitted = new ArrayList<String>();
        String[] wordList = inpStr.split(" ");
        for (String word : wordList) {
            ListStrSplitted.add(word);
        }
        return ListStrSplitted;
    }

}
